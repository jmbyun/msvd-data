
## Microsoft Research Video Description Corpus Processor

Follow the instruction below to download and preprocess the dataset.

### Prepare corpus

Make `{project_root}/config.json` and copy the content from `{project_root}/config.example.json`.
Set `data_path`'s value to store MSVD data.

```json
{
  "data_path": "E:/Research/msvd"
}
```

Download MSVD corpus from the [link](https://www.microsoft.com/en-us/download/details.aspx?id=52422), and save this CSV file as `{data_path}/corpus/corpus.csv`.

### Install dependencies

#### Python

Use Python 3.6.x. Install dependencies in `{project_root}/requirements.txt`.

#### ffmpeg

Install ffmpeg 3.4. Let it accessible by `$ ffmpeg` in the shell.

### Download video files from YouTube

Run downloader script.

```bash
$ python download_videos.py
```

Video files will be downloaded in `{data_path}/video`. The video file path for each YouTube video id is retrievable by reading video-filename file at `{data_path}/video-filename/{youtube_id}.json`. If there is a video file, the video-filename file's content should look like:

```json
{"filename": "{video_filename}"}
```

Read video file at `{data_path}/video/{video_filename}`.

If video file is unavailable on YouTube server, video-filename file's content should look like:

```json
{"error_code": "{error_code}"}
```

### Extract frames from downloaded video files

Run extractor script.

```bash
$ python extract_frames.py
```
Frame images will be saved at `{data_path}/video-frame-image/video_{youtube_id}` as `{frame_index}.jpg`.
